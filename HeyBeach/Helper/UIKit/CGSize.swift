//
//  CGSize.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import UIKit
import AVFoundation

public extension CGSize {
    public func aspectRatioSize(forWidth width: CGFloat) -> CGSize {
        let boundingRect = CGRect(
            x: 0,
            y: 0,
            width: width,
            height: CGFloat(MAXFLOAT)
        )
        let rect = AVMakeRect(
            aspectRatio: self,
            insideRect: boundingRect
        )
        return rect.size
    }
}
