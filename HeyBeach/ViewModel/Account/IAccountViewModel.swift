//
//  IAccountViewModel.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

protocol IAccountViewModel {
    
    var user: User? { get }
    
    init (_ interactor: IAccountInteractor)
    func fetchUser (completion: @escaping () -> Void)
    func logout (completion: @escaping (Bool) -> Void)
}
