//
//  AccountViewModel.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

class AccountViewModel: IAccountViewModel {
    
    var user: User? = nil
    
    private var interactor: IAccountInteractor!
    
    required init(_ interactor: IAccountInteractor) {
        self.interactor = interactor
    }
    
    func fetchUser(completion: @escaping () -> Void) {
        interactor.user { [weak self] (result) in
            self?.user = result?.value
            completion()
        }
    }
    
    func logout(completion: @escaping (Bool) -> Void) {
        interactor.logout { [weak self] (result) in
            let success = result?.isSuccess ?? false
            
            if success {
                self?.user = nil
            }
            
            completion(success)
        }
    }
}
