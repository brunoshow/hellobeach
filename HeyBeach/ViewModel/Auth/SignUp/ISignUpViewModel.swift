//
//  ISignUpViewModel.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

protocol ISignUpViewModel {
    init (_ interactor: IAuthInteractor)
    func register (user: String, pass: String, completion: @escaping (Bool) -> Void)
    var errorMessage: String? { get }
}
