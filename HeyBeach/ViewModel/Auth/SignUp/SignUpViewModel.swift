//
//  SignUpViewModel.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

class SignUpViewModel: ISignUpViewModel {
    var errorMessage: String?
    
    private var interactor: IAuthInteractor!
    
    required init(_ interactor: IAuthInteractor) {
        self.interactor = interactor
    }
    
    func register(user: String, pass: String, completion: @escaping (Bool) -> Void) {
        interactor.register(user: user, pass: pass) { [weak self] (response) in
            guard let response = response else {
                completion(false)
                return
            }
            
            self?.errorMessage = response.error?.localizedDescription
            
            completion(response.isSuccess)
        }
    }
}
