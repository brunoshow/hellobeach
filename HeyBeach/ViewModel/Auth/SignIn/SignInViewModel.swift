//
//  SignInViewModel.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

class SignInViewModel: ISignInViewModel {
    
    private var interactor: IAuthInteractor!
    
    required init(_ interactor: IAuthInteractor) {
        self.interactor = interactor
    }
    
    func login(user: String, pass: String, completion: @escaping (Bool) -> Void) {
        interactor.login(user: user, pass: pass) { (response) in
            completion(response?.isSuccess ?? false)
        }
    }
}
