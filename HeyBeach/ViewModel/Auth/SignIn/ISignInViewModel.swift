//
//  ISignInViewModel.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

protocol ISignInViewModel {
    init (_ interactor: IAuthInteractor)
    func login (user: String, pass: String, completion: @escaping (Bool) -> Void)
}
