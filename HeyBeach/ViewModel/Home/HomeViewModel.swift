//
//  HomeViewModel.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

class HomeViewModel: IHomeViewModel {
    private var interactor: IHomeInteractor!
    var beaches:[Beach] = []
    
    private var page = 0
    private let limit = 6
    
    required init(_ interactor: IHomeInteractor) {
        self.interactor = interactor
    }
    
    func fetchData (completion: @escaping () -> Void) {
        
        interactor.fetchBeaches(page: page) { [weak self] (response) in
            
            guard let _ = response?.isSuccess,
                let result = response?.value else {
                    print("Error")
                    return
            }
            
            guard result.count > 0 else {
                completion()
                return
            }
            
            self?.page += 1
            self?.beaches.append(contentsOf: result)
            
            completion()
        }
    }
}
