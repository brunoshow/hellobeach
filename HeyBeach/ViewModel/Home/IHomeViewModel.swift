//
//  IHomeViewModel.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

protocol IHomeViewModel {
    var beaches:[Beach] { get }
    
    init (_ interactor: IHomeInteractor)
    func fetchData (completion: @escaping () -> Void)
}
