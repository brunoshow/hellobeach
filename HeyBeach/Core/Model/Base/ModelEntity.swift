//
//  ModelEntity.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/23/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

typealias ModelEntity = Codable
