//
//  APICustomError.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

struct APICustomError: ModelEntity {
    let errors: Errors
    let message, name: String
}

struct Errors: ModelEntity {
    let password: Password
}

struct Password: ModelEntity {
    let message, name: String
    let properties: Properties
    let kind, path, value: String
}

struct Properties: ModelEntity {
    let minlength: Int
    let type, message, path, value: String
}
