//
//  User.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

struct User: ModelEntity {
    let id, email: String
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case email
    }
}
