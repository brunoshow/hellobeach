//
//  Beach.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

struct Beach: ModelEntity {
    let id, name, url, width: String
    let height: String
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, url, width, height
    }
}
