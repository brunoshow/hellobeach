//
//  NetworkService.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

class NetworkService: INetworkService {
    var newtworkDispatcher: INetworkDispatcher = NetworkDispatcher.instance
    
    func execute(endpoint: APIBeachEndpoint, onComplete: @escaping (DataResponse<Void>) -> Void) {
        newtworkDispatcher.dispatch(request: endpoint, onSuccess: { (responseData: Data, urlResponse: HTTPURLResponse) in
            let response = DataResponse<Void>(result: .success(()), urlResponse: urlResponse)
            onComplete(response)
            
        }) { (error: Error, urlResponse: HTTPURLResponse?) in
            DispatchQueue.main.async {
                let response = DataResponse<Void>(result: .failure(error), urlResponse: urlResponse)
                onComplete(response)
            }
        }
    }
    
    func execute<T>(endpoint: APIBeachEndpoint, onComplete: @escaping (DataResponse<T>) -> Void) where T: ModelEntity {
        
        newtworkDispatcher.dispatch(
            request: endpoint,
            onSuccess: { (responseData: Data, urlResponse: HTTPURLResponse) in
                do {
                    
                    let jsonDecoder = JSONDecoder()
                    let result = try jsonDecoder.decode(T.self, from: responseData)
                    DispatchQueue.main.async {
                        let response = DataResponse<T>(result: .success(result), urlResponse: urlResponse)
                        onComplete(response)
                    }
                } catch let error {
                    
                    DispatchQueue.main.async {
                        let response = DataResponse<T>(result: .failure(error), urlResponse: urlResponse)
                        onComplete(response)
                    }
                }
        },
            onError: { (error: Error, urlResponse: HTTPURLResponse?) in
                DispatchQueue.main.async {
                    let response = DataResponse<T>(result: .failure(error), urlResponse: urlResponse)
                    onComplete(response)
                }
        }
        )
    }
}

extension NetworkService {
    
    private static var instance: INetworkService!
    static func current() -> INetworkService {
        if instance == nil {
            instance = NetworkService()
        }
        return instance
    }
}
