//
//  INetworkService.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

protocol INetworkService {
    var newtworkDispatcher: INetworkDispatcher { get }
    
    func execute<T>(endpoint: APIBeachEndpoint, onComplete: @escaping (DataResponse<T>) -> Void) where T: ModelEntity
    
    func execute(endpoint: APIBeachEndpoint, onComplete: @escaping (DataResponse<Void>) -> Void)
}
