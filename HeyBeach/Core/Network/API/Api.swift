//
//  Api.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

struct API {
    static let baseURLString = "http://techtest.lab1886.io:3000"
    
    ////////////////////////////////////////////////////////////////
    //MARK:-
    //MARK:Endpoints
    //MARK:-
    ////////////////////////////////////////////////////////////////
    
    static let trendingPath = "/v1/gifs/trending"
    static let login = "/user/login"
    static let register = "/user/register"
    static let logout = "/user/logout"
    static let user = "/user/me"
    static let beaches = "/beaches"
}

enum APIError: Error {
    case invalidURL
    case noData
    case generic
    case invalidToken
    case noConnection
}

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case patch = "PATCH"
}
