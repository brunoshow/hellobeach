//
//  APIEndpoint.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

enum APIBeachEndpoint {
    case login(user: String, pass: String)
    case register(user: String, pass: String)
    case logout(token: String)
    case user(token: String)
    
    case fetchBeaches(page: Int)
}

extension APIBeachEndpoint {
    
    var url: String {
        switch self {
        case .fetchBeaches(_):
            return "\(API.baseURLString)\(API.beaches)"
        case .login(_, _):
            return "\(API.baseURLString)\(API.login)"
        case .register(_, _):
            return "\(API.baseURLString)\(API.register)"
        case .logout(_):
            return "\(API.baseURLString)\(API.logout)"
        case .user(_):
            return "\(API.baseURLString)\(API.user)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .fetchBeaches(_),
             .user(_):
            return .get
            
        case .login(_, _),
             .register(_, _):
            return .post
            
        case .logout(_):
            return .delete
        }
    }
    
    var params: [String: Any?]? {
        switch self {
        case .fetchBeaches(let page):
            return [
                "page": "\(page)"
            ]
        case .login(let user, let pass):
            return [
                "email": "\(user)",
                "password":"\(pass)"
            ]
        case .register(let user, let pass):
            return [
                "email": "\(user)",
                "password":"\(pass)"
            ]
        case .logout(_),
             .user:
            return nil
        }
    }
    
    var headers: [String: String]? {
        switch self {
        case .user(let token),
             .logout(let token):
            return [
                "Content-Type": "application/json",
                "Cache-Control": "no-cache",
                "x-auth": token
            ]
        case .fetchBeaches(_):
            return nil
        case .login(_, _),
             .register(_, _):
            return [
                "Content-Type": "application/json",
                "Cache-Control": "no-cache"
            ]
        }
    }
}
