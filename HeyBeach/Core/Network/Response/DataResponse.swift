//
//  DataResponse.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

public struct DataResponse<Value> {
    
    public let result: Result<Value>
    public let urlResponse: HTTPURLResponse?
    
    init(result: Result<Value>, urlResponse: HTTPURLResponse?)
    {
        self.result = result
        self.urlResponse = urlResponse
    }
}
