//
//  INetworkDispatcher.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

protocol INetworkDispatcher {
    func dispatch(request: APIBeachEndpoint, onSuccess: @escaping (Data, HTTPURLResponse) -> Void, onError: @escaping (Error, HTTPURLResponse?) -> Void)
}
