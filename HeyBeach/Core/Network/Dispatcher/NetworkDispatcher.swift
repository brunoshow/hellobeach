//
//  NetworkDispatcher.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

struct NetworkDispatcher: INetworkDispatcher {
    public static let instance = NetworkDispatcher()
    private init() {}
    
    public func dispatch(request: APIBeachEndpoint, onSuccess: @escaping (Data, HTTPURLResponse) -> Void, onError: @escaping (Error, HTTPURLResponse?) -> Void) {
        guard let _ = URL(string: request.url) else {
            onError(APIError.invalidURL, nil)
            return
        }
        let urlComp = NSURLComponents(string: request.url)!
        if request.method == .get {
            
            var items = [URLQueryItem]()
            
            if let params = request.params {
                for (key,value) in params {
                    items.append(URLQueryItem(name: key, value: value as? String))
                }
                
                items = items.filter{!$0.name.isEmpty}
                
                if !items.isEmpty {
                    urlComp.queryItems = items
                }
            }
        }
        var urlRequest = URLRequest(url: urlComp.url!)
        urlRequest.httpMethod = request.method.rawValue
        
        do {
            if let params = request.params {
                let jsonData = try JSONSerialization.data(withJSONObject: params, options: [])
                urlRequest.httpBody = jsonData
            }
        } catch let error {
            onError(error, nil)
            return
        }
        
        if let headers = request.headers {
            urlRequest.allHTTPHeaderFields = headers
        }
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            
            guard let response = response as? HTTPURLResponse else {
                onError(APIError.noData, nil)
                return
            }
            
            if let error = error {
                onError(error, response)
                return
            }
            guard let _data = data else {
                onError(APIError.noData, response)
                return
            }
            
            let statusCode = response.statusCode
            
            if !(200..<300).contains(statusCode) {
                
                guard statusCode != 401 else {
                    onError(APIError.invalidToken, response)
                    return
                }
                
                onError(APIError.generic, response)
                return
            }
            
            onSuccess(_data, response)
            }.resume()
    }
}
