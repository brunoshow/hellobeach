//
//  StorageKeys.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

enum StorageKeys: String {
    case AuthToken = "x-auth"
}
