//
//  HomeInteractor.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

class HomeInteractor: IHomeInteractor {
    var networkService: INetworkService!
    
    required init(with service: INetworkService) {
        self.networkService = service
    }
    
    func fetchBeaches(page: Int, _ completion: @escaping (Result<[Beach]>?) -> Void) {
        
        self.networkService.execute(endpoint: APIBeachEndpoint.fetchBeaches(page: page),
                                    onComplete: { (response: DataResponse<[Beach]>) in
                                        completion(response.result)
        })
    }
}
