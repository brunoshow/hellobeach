//
//  IHomeInteractor.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

protocol IHomeInteractor {
    var networkService: INetworkService! { get }
    init (with service: INetworkService)
    
    func fetchBeaches(page: Int, _ completion: @escaping (Result<[Beach]>?) -> Void)
}
