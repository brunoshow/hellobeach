//
//  IAuthInteractor.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

protocol IAuthInteractor {
    var networkService: INetworkService! { get }
    init (with service: INetworkService)
    
    func login(user: String, pass: String, _ completion: @escaping (Result<User>?) -> Void)
    func register(user: String, pass: String, _ completion: @escaping (Result<User>?) -> Void)
}
