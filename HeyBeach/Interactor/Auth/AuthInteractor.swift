//
//  AuthInteractor.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

class AuthInteractor: IAuthInteractor {
    var networkService: INetworkService!
    
    required init(with service: INetworkService) {
        self.networkService = service
    }
    
    func login(user: String, pass: String, _ completion: @escaping (Result<User>?) -> Void) {
        
        self.networkService.execute(endpoint: APIBeachEndpoint.login(user: user, pass: pass),
                                    onComplete: { [weak self] (response: DataResponse<User>) in
                                        if let token = response.urlResponse?.allHeaderFields["x-auth"] as? String,
                                            response.result.isSuccess {
                                            self?.saveAuthorizationInfo(user: user, pass: pass, token: token)
                                        }
                                        
                                        completion(response.result)
        })
    }
    
    func register(user: String, pass: String, _ completion: @escaping (Result<User>?) -> Void) {
        
        self.networkService.execute(endpoint: APIBeachEndpoint.register(user: user, pass: pass),
                                    onComplete: { [weak self] (response: DataResponse<User>) in
                                        if let token = response.urlResponse?.allHeaderFields["x-auth"] as? String,
                                            response.result.isSuccess {
                                            self?.saveAuthorizationInfo(user: user, pass: pass, token: token)
                                        }
                                        
                                        completion(response.result)
        })
    }
    
    // TODO Move it for a different Class which will manage it
    private func saveAuthorizationInfo (user: String, pass: String, token: String) {
        try? KeychainPasswordItem(service: KeychainConfiguration.serviceName, account: user).savePassword(pass)
        
        UserDefaults.standard.set(token, forKey: StorageKeys.AuthToken.rawValue)
    }
}
