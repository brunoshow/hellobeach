//
//  AccountInteractor.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

class AccountInteractor: IAccountInteractor {
    
    var networkService: INetworkService!
    
    required init(with service: INetworkService) {
        self.networkService = service
    }
    
    func user(_ completion: @escaping (Result<User>?) -> Void) {

        self.networkService.execute(endpoint: APIBeachEndpoint.user(token: getToken()),
                                    onComplete: { (response: DataResponse<User>) in
                                        completion(response.result)
        })
    }
    
    func logout(_ completion: @escaping (Result<Void>?) -> Void) {
        
        self.networkService.execute(endpoint: APIBeachEndpoint.logout(token: getToken()),
                                    onComplete: { (response: DataResponse<Void>) in
                                        completion(response.result)
        })
    }
    
    private func getToken() -> String {
        return UserDefaults.standard.string(forKey: StorageKeys.AuthToken.rawValue) ?? ""
    }
}
