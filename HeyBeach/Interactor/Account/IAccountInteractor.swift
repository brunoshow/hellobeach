//
//  IAccountInteractor.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

protocol IAccountInteractor {
    var networkService: INetworkService! { get }
    init (with service: INetworkService)
    
    func user( _ completion: @escaping (Result<User>?) -> Void)
    func logout( _ completion: @escaping (Result<Void>?) -> Void)
}
