//
//  MainTab.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import UIKit

class MainTab: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewControllers = [HomeBuilder().main(), AccountBuilder().main()]
    }
}
