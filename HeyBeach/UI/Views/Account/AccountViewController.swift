//
//  AccountViewController.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {
    
    var viewModel: IAccountViewModel!
    
    @IBOutlet weak var loggedView: UIView!
    @IBOutlet weak var loggedOutView: UIView!
    @IBOutlet weak var identificationLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        guard let _ = viewModel.user else {
            viewModel.fetchUser { [weak self] in
                DispatchQueue.main.async {
                    self?.updateUserData()
                }
            }
            return
        }
        updateUserData()
    }
    
    private func setUserInfo(user: User) {
        identificationLabel.text = user.id
        emailLabel.text = user.email
    }
    
    private func clearUserInfo() {
        identificationLabel.text = ""
        emailLabel.text = ""
    }
    
    private func requireAuthentication() {
        loggedView.isHidden = true
        loggedOutView.isHidden = false
    }
    
    private func displayUserData() {
        loggedView.isHidden = false
        loggedOutView.isHidden = true
    }
    
    private func updateUserData() {
        guard let user = viewModel.user else {
            self.clearUserInfo()
            self.requireAuthentication()
            return
        }
        
        setUserInfo(user: user)
        displayUserData()
    }
    
    
    ////////////////////////////////////////////////////////////////
    //MARK:-
    //MARK:Buttons Actions
    //MARK:-
    ////////////////////////////////////////////////////////////////

    @IBAction func SignUp(_ sender: UIButton) {
        let viewController = SignUpBuilder().main()
        self.present(viewController, animated: false, completion: nil)
    }
    
    @IBAction func SignIn(_ sender: UIButton) {
        let viewController = SignInBuilder().main()
        self.present(viewController, animated: false, completion: nil)
    }
    
    @IBAction func logout(_ sender: Any) {
        viewModel.logout { [weak self] (loggedOut) in
            guard loggedOut else {
                return
            }
            DispatchQueue.main.async {
                self?.updateUserData()
            }
        }
    }
}
