//
//  HomeViewController+Delegates.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, ContentDynamicLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.beaches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.row == viewModel.beaches.count - 1) {
            self.loadBeach()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BeachCell.identifer, for: indexPath) as? BeachCell
            else { preconditionFailure("Failed to load collection view cell") }
        
        if !viewModel.beaches.isEmpty {
            let beach = viewModel.beaches[indexPath.row]
            
            cell.setContent(url: "\(API.baseURLString)/\(beach.url)", name: beach.name, id: beach.id)
        }
        
        return cell
    }
    
    func cellSize(indexPath: IndexPath, cellWidth: CGFloat) -> CGSize {
        let beach = viewModel.beaches[indexPath.row]
        guard let height = Double(beach.height),
            let width = Double(beach.width) else {
                return .zero
        }
        let size = CGSize(width: width, height: height).aspectRatioSize(forWidth: cellWidth)
        return size
    }
}
