//
//  HomeViewController.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation
import UIKit

class HomeViewController: UIViewController {
    var viewModel: IHomeViewModel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        setCollectionView()
        loadBeach()
    }
    
    func loadBeach() {
        let numberOfItems = self.viewModel.beaches.count
        self.viewModel.fetchData() { [weak self] in
            guard let results = self?.viewModel.beaches.count else {
                return
            }
            if numberOfItems < results {
                DispatchQueue.main.async {
                    self?.collectionView.reloadData()
                }
            }
        }
    }
    
    private func setCollectionView() {
        let contentFlowLayout: ContentDynamicLayout = PinterestStyleFlowLayout()
        
        contentFlowLayout.delegate = self
        contentFlowLayout.contentPadding = ItemsPadding(horizontal: 10, vertical: 10)
        contentFlowLayout.cellsPadding = ItemsPadding(horizontal: 8, vertical: 8)
        contentFlowLayout.contentAlign = .left
        
        collectionView.collectionViewLayout = contentFlowLayout
        
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.alwaysBounceVertical = true
        collectionView.indicatorStyle = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(BeachCell.self, forCellWithReuseIdentifier: BeachCell.identifer)
    }
}
