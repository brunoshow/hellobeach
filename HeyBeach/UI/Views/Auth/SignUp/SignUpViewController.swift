//
//  SignUpViewController.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    var onViewWillDisappear: (()->())?
    var viewModel: ISignUpViewModel!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func register(_ sender: UIButton) {
        guard let email = emailTextField.text else {
            showError(message: "Email Required")
            return
        }
        
        guard let pass = passwordTextField.text else {
            showError(message: "Password Required")
            return
        }
        
        viewModel.register(user: email, pass: pass) { [weak self] (isLogged) in
            if isLogged {
                self?.dismiss(animated: true, completion: nil)
            } else {
                self?.showError(message: "Validation Error. \nPlease verify your email addres.\nPassword must have leght 5")
            }
        }
    }
    
    func showError(message: String) {
        let alert = UIAlertController(title: "Hello Beaches", message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
}
