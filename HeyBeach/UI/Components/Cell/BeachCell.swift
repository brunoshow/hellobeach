//
//  BeachCell.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import UIKit

class BeachCell: UICollectionViewCell {
    static let identifer = "beachCell"
    
    private var imageView = AsyncImageView()
    private var nameLabel = UILabel()
    private var id: String?
    
    fileprivate var downloadTask: URLSessionDataTask?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
        self.autoresizesSubviews = true
        
        setImageView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
//        downloadTask?.cancel()
        imageView.image = nil
        nameLabel.text = nil
        id = nil
    }
    
    func setContent(url: String, name: String, id: String) {
        self.id = id
        self.nameLabel.text = name
        
        downloadTask = imageView.loadAsyncFrom(url: url, placeholder: UIImage(named: "beach_placeholder"))
    }
    
    private func setImageView() {
        imageView.frame = self.bounds
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(imageView)
        
        nameLabel.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 30)
        nameLabel.contentMode = .bottomLeft
        nameLabel.textAlignment = .left
        nameLabel.clipsToBounds = true
        self.addSubview(nameLabel)
    }
}
