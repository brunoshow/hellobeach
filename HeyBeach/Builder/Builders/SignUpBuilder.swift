//
//  SignUpBuilder.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation
import UIKit

class SignUpBuilder: AppBuilder {
    var networkService: INetworkService!
    
    func main() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SignUpView") as! SignUpViewController
        let interactor : IAuthInteractor = AuthInteractor(with: networkService)
        let viewModel : ISignUpViewModel = SignUpViewModel(interactor)
        viewController.viewModel = viewModel
        return viewController
    }
}
