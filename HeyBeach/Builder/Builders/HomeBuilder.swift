//
//  HomeBuilder.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation
import UIKit

class HomeBuilder: AppBuilder {
    var networkService: INetworkService!
    
    func main() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeView") as! HomeViewController
        let interactor : IHomeInteractor = HomeInteractor(with: networkService)
        let viewModel : IHomeViewModel = HomeViewModel(interactor)
        viewController.viewModel = viewModel
        let tab = UITabBarItem(title: "Beaches", image: UIImage(named: "tab_beaches.png"), selectedImage: UIImage(named: "tab_beaches_selected.png"))
        viewController.tabBarItem = tab
        return viewController
    }
}
