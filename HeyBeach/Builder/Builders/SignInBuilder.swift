//
//  SignInBuilder.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation
import UIKit

class SignInBuilder: AppBuilder {
    var networkService: INetworkService!
    
    func main() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SignInView") as! SignInViewController
        let interactor : IAuthInteractor = AuthInteractor(with: networkService)
        let viewModel : ISignInViewModel = SignInViewModel(interactor)
        viewController.viewModel = viewModel
        return viewController
    }
}
