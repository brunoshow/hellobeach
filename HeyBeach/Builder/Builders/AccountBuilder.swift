//
//  AccountBuilder.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation
import UIKit

class AccountBuilder: AppBuilder {
    var networkService: INetworkService!
    
    func main() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AccountView") as! AccountViewController
        let interactor : IAccountInteractor = AccountInteractor(with: networkService)
        let viewModel : IAccountViewModel = AccountViewModel(interactor)
        viewController.viewModel = viewModel
        let tab = UITabBarItem(title: "Account", image: UIImage(named: "tab_account.png"), selectedImage: UIImage(named: "tab_account_selected.png"))
        viewController.tabBarItem = tab
        return viewController
    }
}
