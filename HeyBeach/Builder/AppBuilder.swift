//
//  AppBuilder.swift
//  HeyBeach
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation
import UIKit

protocol AppBuilder {
    var networkService: INetworkService! { get }
    
    func main() -> UIViewController
}

extension AppBuilder {
    var networkService: INetworkService {
        return NetworkService.current()
    }
}
