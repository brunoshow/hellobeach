[![Build Status](https://app.bitrise.io/app/bc1ad3d453c52b86/status.svg?token=Iy2NiTYOzFJdrOSFnCmIgg&branch=Dev)](https://app.bitrise.io/app/bc1ad3d453c52b86)

[![codebeat badge](https://codebeat.co/badges/6e7fb0a4-e9c6-451e-bf6d-89a6a37487da)](https://codebeat.co/projects/gitlab-com-brunoshow-hellobeach-dev)

[![codecov](https://codecov.io/gl/brunoshow/hellobeach/branch/master/graph/badge.svg)](https://codecov.io/gl/brunoshow/hellobeach)

# Hello Beach

### 📱 Hello Beach - Bruno Show

Project was create on **XCode Version 10.1** :

#### Check List
- [x] The user should see a list of images fetched from the HeyBeach API. (Initially just display the first page from the api)
- [x] A user should be able to register/login/logout inside the app.
- [x] Ensure the design interface is responsive and functional on mobile, desktop and/or tablets. [Screenshots](#Screenshots) [TradeOff](#Trade-off's)
- [x] Include the image title with each image.
- [x] Implement some sort of infinite scrolling (Get all the images using the paginated api).
- [x] Implement some sort of image caching.
- [x] The image grid can be flexible, respecting the images aspect ratio (non-fixed size grid, check the wireframe, eg. Pinterest style)

#### Archtecture
- Respecting MVVM Archtecture pattern
- Main Modules
  - Auth
  - Account
  - Home

- Layers
  - Model
  - View
  - ViewModel
  - Interactor
  - Builder
  - Network
  - Persistence

#### Things to Improove / Thoghts
- Create a user session Layer
- Maybe Split the auth interactor into signIn and SignUp
- Or Even put Logout into authInteractor
- Change presentation of login and register
- Add layer for ErrorHandling
- REmove showError duplication
- Display error from API
- Add Validation for UITextFields
- Add UITests
- Add More Unit tests curently its (57.5% of test coverage)[Screenshots](#Screenshots)
- Add mechanism for auto re-authentication if token expire
- Add Navigation Way back for SignIn and SignUp
- Add loader at Account Screen

#### Trade-off's
- Block rotation of device, for handle less edge cases
- Missing Error Handling

#### Tests / Coverage
Tests were focused in the following Layers
- ViewModel
- Interactor
- Builder
- View

#### UI
Project has no interfaces
Lack of Test on network layer
No usage of external Libs

#### Navigation Flow
1. App Start
2. Beaches List is Visible as 1st table view Item
3. Second Item is Account Info (tap)
4. If user isn't authenticated it asks for Login / Register
5. Login and Register has same behavior if token return, it shows user the previous screen with some informations collected from user endpoint

#### Sources that helped to archive some of the results
https://developer.apple.com/library/archive/samplecode/GenericKeychain/Introduction/Intro.html
https://github.com/rubygarage/collection-view-layouts
https://github.com/MagicLab-team/PinterestLayout

#### ScreenShots

![10](ScreenShot/Screen Shot 2019-01-16 at 17.02.01.png)
![9](ScreenShot/Screen Shot 2019-01-16 at 17.02.09.png)
![0](ScreenShot/Screen Shot 2019-01-16 at 17.04.25.png)
![1](ScreenShot/Screen Shot 2019-01-16 at 17.04.39.png)
![2](ScreenShot/Screen Shot 2019-01-16 at 17.02.14.png)
![3](ScreenShot/Screen Shot 2019-01-16 at 17.02.18.png)
![4](ScreenShot/Screen Shot 2019-01-16 at 17.03.32.png)
![5](ScreenShot/Screen Shot 2019-01-16 at 17.04.17.png)

![6](ScreenShot/Screen Shot 2019-01-16 at 17.05.47.png)

