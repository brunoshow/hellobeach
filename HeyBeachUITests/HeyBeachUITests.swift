//
//  HeyBeachUITests.swift
//  HeyBeachUITests
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import XCTest

class HeyBeachUITests: XCTestCase {
    
    let app =  XCUIApplication()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    func testTabBeachesIsVisible() {
        let beachTab = app.tabBars.buttons["Beaches"]
        let exist = beachTab.waitForExistence(timeout: TimeInterval(100))
        XCTAssertTrue(exist)
    }
    
    func testTabAccountIsVisible() {
        let accountTab = app.tabBars.buttons["Account"]
        let exist = accountTab.waitForExistence(timeout: TimeInterval(100))
        XCTAssertTrue(exist)
    }
}
