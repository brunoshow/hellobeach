//
//  AuthInteractorTest.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import XCTest
@testable import HeyBeach

class AuthInteractorTest: XCTestCase {
    
    var networkService: INetworkService!
    var interactor: IAuthInteractor!
    
    
    ////////////////////////////////////////////////////////////////
    //MARK:-
    //MARK:Login Tests
    //MARK:-
    ////////////////////////////////////////////////////////////////

    func testLogin() {
        self.networkService = NetworkServiceStub(stub: .login)
        self.interactor = AuthInteractor(with: networkService)
        
        interactor.login(user: "valid", pass: "valid", { (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            XCTAssertTrue(result.isSuccess)
        })
    }
    
    func testLoginFail() {
        self.networkService = NetworkServiceStub(stub: .login_fail)
        self.interactor = AuthInteractor(with: networkService)
        
        interactor.login(user: "invalid", pass: "invalid", { (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            
            guard let error = result.error as? APIError else {
                assertionFailure()
                return
            }
            
            XCTAssertEqual(error, APIError.invalidURL)
            XCTAssertTrue(result.isFailure)
        })
    }
    
    func testLoginWithError() {
        self.networkService = NetworkServiceStub(stub: .login, error: APIError.generic)
        self.interactor = AuthInteractor(with: networkService)
        
        interactor.login(user: "error", pass: "error", { (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            guard let error = result.error as? APIError else {
                assertionFailure()
                return
            }
            
            XCTAssertEqual(error, APIError.generic)
            XCTAssertTrue(result.isFailure)
        })
    }
    
    
    ////////////////////////////////////////////////////////////////
    //MARK:-
    //MARK:Register Tests
    //MARK:-
    ////////////////////////////////////////////////////////////////
    
    func testRegister() {
        self.networkService = NetworkServiceStub(stub: .register)
        self.interactor = AuthInteractor(with: networkService)
        
        interactor.register(user: "valid", pass: "valid", { (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            XCTAssertTrue(result.isSuccess)
        })
    }
    
    func testRegisterFail() {
        self.networkService = NetworkServiceStub(stub: .register_fail)
        self.interactor = AuthInteractor(with: networkService)
        
        interactor.register(user: "invalid", pass: "invalid", { (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            
            guard let error = result.error as? APIError else {
                assertionFailure()
                return
            }
            
            XCTAssertEqual(error, APIError.invalidURL)
            XCTAssertTrue(result.isFailure)
        })
    }
    
    func testRegisterWithError() {
        self.networkService = NetworkServiceStub(stub: .register, error: APIError.generic)
        self.interactor = AuthInteractor(with: networkService)
        
        interactor.register(user: "error", pass: "error", { (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            guard let error = result.error as? APIError else {
                assertionFailure()
                return
            }
            
            XCTAssertEqual(error, APIError.generic)
            XCTAssertTrue(result.isFailure)
        })
    }
}
