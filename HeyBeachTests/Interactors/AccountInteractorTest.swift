//
//  AccountInteractorTest.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation
import XCTest
@testable import HeyBeach

class AccountInteractorTest: XCTestCase {
    
    var networkService: INetworkService!
    var interactor: IAccountInteractor!
    
    func testFetchUser() {
        self.networkService = NetworkServiceStub(stub: .user)
        self.interactor = AccountInteractor(with: networkService)
        
        interactor.user({ (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            XCTAssertTrue(result.isSuccess)
        })
    }
    
    func testFetchUserFail() {
        self.networkService = NetworkServiceStub(stub: .user_fail)
        self.interactor = AccountInteractor(with: networkService)
        
        interactor.user({ (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            XCTAssertTrue(result.isFailure)
        })
    }
    
    func testLogout() {
        self.networkService = NetworkServiceStub(stub: .logout)
        self.interactor = AccountInteractor(with: networkService)
        
        interactor.logout { (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            XCTAssertTrue(result.isSuccess)
        }
    }
    
    func testLogoutFail() {
        self.networkService = NetworkServiceStub(stub: .logout_fail)
        self.interactor = AccountInteractor(with: networkService)
        
        interactor.logout { (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            XCTAssertTrue(result.isFailure)
        }
    }
}
