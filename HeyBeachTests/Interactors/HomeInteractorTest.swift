//
//  HomeInteractorTest.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation
import XCTest
@testable import HeyBeach

class HomeInteractorTest: XCTestCase {
    
    var networkService: INetworkService!
    var interactor: IHomeInteractor!
    
    func testFetchBeach() {
        self.networkService = NetworkServiceStub(stub: .beaches_page_1)
        self.interactor = HomeInteractor(with: networkService)

        interactor.fetchBeaches(page: 0) { (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            XCTAssertTrue(result.isSuccess)
        }
    }
    
    func testFetchBeachWithError() {
        self.networkService = NetworkServiceStub(stub: .beaches_error)
        self.interactor = HomeInteractor(with: networkService)
        
        interactor.fetchBeaches(page: 0) { (result) in
            guard let result = result else {
                assertionFailure()
                return
            }
            XCTAssertTrue(result.isFailure)
        }
    }
}
