//
//  SignInBuilderTest.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import XCTest
@testable import HeyBeach

class SignInBuilderTest: XCTestCase {
    
    var viewController: UIViewController!
    
    override func setUp() {
        super.setUp()
        viewController = SignInBuilder().main()
    }
    
    func testViewControllerType() {
        XCTAssert(viewController is SignInViewController)
    }
    
}
