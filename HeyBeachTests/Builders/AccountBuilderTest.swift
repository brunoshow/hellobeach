//
//  AccountBuilderTest.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import XCTest
@testable import HeyBeach

class AccountBuilderTest: XCTestCase {
    
    var builder: AppBuilder!
    var viewController: UIViewController!
    
    override func setUp() {
        super.setUp()
        viewController = AccountBuilder().main()
    }
    
    func testViewControllerType() {
        XCTAssert(viewController is AccountViewController)
    }
    
}
