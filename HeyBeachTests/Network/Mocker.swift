//
//  Mocker.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation
@testable import HeyBeach

class Mocker {
    
    func mock<T>(stub: StubJson) -> T? where T : ModelEntity {
        let result: T? = getCodableFromJsonFile(stub: stub)
        return result
    }
    
    func getResult<T>(stub: StubJson) -> T? where T : ModelEntity {
        let result: T? = getCodableFromJsonFile(stub: stub)
        return result
    }
    
    private func getCodableFromJsonFile<T>(stub: StubJson) -> T? where T : ModelEntity {
        guard let data = self.getDataFromFile(filename: stub.rawValue) else{
            return nil
        }
        
        let decoder = JSONDecoder()
        let jsonData = try? decoder.decode(T.self, from: data)
        
        return jsonData
    }
    
    func getDataFromFile(filename: String) -> Data? {
        guard let url = Bundle(for: type(of: self)).url(forResource: filename, withExtension: "json") else {
            return nil
        }
        let data = try? Data(contentsOf: url)
        
        return data
    }
}
