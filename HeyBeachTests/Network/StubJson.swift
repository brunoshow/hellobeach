//
//  StubJson.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation

enum StubJson: String {
    
    case register,
    register_fail,
    login,
    login_fail,
    logout,
    logout_fail,
    user,
    user_fail,
    beaches_page_1,
    beaches_page_2,
    beaches_empty,
    beaches_error
}
