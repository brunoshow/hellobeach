//
//  NetworkStub.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/15/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import Foundation
@testable import HeyBeach

class MockDispatcher: INetworkDispatcher {
    func dispatch(request: APIBeachEndpoint, onSuccess: @escaping (Data, HTTPURLResponse) -> Void, onError: @escaping (Error, HTTPURLResponse?) -> Void) {
        // Empty
    }
}

class NetworkServiceStub: INetworkService {
    var newtworkDispatcher: INetworkDispatcher = MockDispatcher()
    
    private let mocker = Mocker()
    private var stub: StubJson
    private var error: Error?
    private var urlResponse: HTTPURLResponse?

    init(stub: StubJson, error: Error? = nil, urlResponse: HTTPURLResponse? = nil) {
        self.stub = stub
        self.error = error
        self.urlResponse = urlResponse
    }
    
    func changeStub(stub: StubJson) {
        self.stub = stub
    }
    
    func execute<T>(endpoint: APIBeachEndpoint, onComplete: @escaping (DataResponse<T>) -> Void) where T : ModelEntity {
        
        guard let error = self.error else {
            let data: T? = mocker.getResult(stub: stub)
            
            guard let result = data else {
                onComplete(DataResponse<T>(result: .failure(APIError.invalidURL), urlResponse: urlResponse))
                return
            }
            
            let response = DataResponse<T>(result: .success(result), urlResponse: urlResponse)
            onComplete(response)
            return
        }
        
        let response = DataResponse<T>(result: .failure(error), urlResponse: urlResponse)
        onComplete(response)
    }
    
    func execute(endpoint: APIBeachEndpoint, onComplete: @escaping (DataResponse<Void>) -> Void) {
        guard let error = self.error else {
            let data = mocker.getDataFromFile(filename: stub.rawValue)
            guard let _ = data else {
                let response = DataResponse<()>(result: .failure(APIError.generic), urlResponse: urlResponse)
                onComplete(response)
                return
            }
            
            let response = DataResponse<()>(result: .success(()), urlResponse: urlResponse)
            onComplete(response)
            
            return
        }
        
        let response = DataResponse<()>(result: .failure(error), urlResponse: urlResponse)
        onComplete(response)
    }
}
