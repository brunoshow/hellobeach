//
//  HomeViewControllerTest.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import XCTest
@testable import HeyBeach

class HomeViewControllerTest: XCTestCase {

    var viewController: HomeViewController!
    
    override func setUp() {
        super.setUp()
        viewController = HomeBuilder().main() as? HomeViewController
        UIApplication.shared.keyWindow!.rootViewController = viewController
    }
    
    func testViewLoaded() {
        XCTAssertNotNil(viewController.view)
    }
    
    func testViewModelExist() {
        XCTAssertNotNil(viewController.viewModel)
    }

}
