//
//  SignInViewControllerTest.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import XCTest
@testable import HeyBeach

class SignInViewControllerTest: XCTestCase {
    
    var viewController: SignInViewController!
    
    override func setUp() {
        super.setUp()
        viewController = SignInBuilder().main() as? SignInViewController
        UIApplication.shared.keyWindow!.rootViewController = viewController
    }
    
    func testViewLoaded() {
        XCTAssertNotNil(viewController.view)
    }
    
    func testViewModelExist() {
        XCTAssertNotNil(viewController.viewModel)
    }
}
