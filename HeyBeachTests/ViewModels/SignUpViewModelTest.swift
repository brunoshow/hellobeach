//
//  SignUpViewModelTest.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import XCTest
@testable import HeyBeach

class SignUpViewModelTest: XCTestCase {
    
    private var networkService: INetworkService!
    private var interactor: IAuthInteractor!
    private var viewModel: ISignUpViewModel!
    private let mocker = Mocker()
    
    override func setUp() {
        self.networkService = NetworkServiceStub(stub: .register)
        self.interactor = AuthInteractor(with: networkService)
        self.viewModel = SignUpViewModel(interactor)
    }
    
    func testLogin() {
        viewModel.register(user: "valid", pass: "valid") { (isRegistered) in
            XCTAssertTrue(isRegistered)
        }
    }
    
    func testUserLoginFail() {
        guard let service = self.networkService as? NetworkServiceStub else {
            assertionFailure()
            return
        }
        
        service.changeStub(stub: .register_fail)
        
        viewModel.register(user: "invalid", pass: "invalid") { (isRegistered) in
            XCTAssertFalse(isRegistered)
        }
    }
}
