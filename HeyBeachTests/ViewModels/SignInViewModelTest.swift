//
//  SignInViewModelTest.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import XCTest
@testable import HeyBeach

class SignInViewModelTest: XCTestCase {
    
    private var networkService: INetworkService!
    private var interactor: IAuthInteractor!
    private var viewModel: ISignInViewModel!
    private let mocker = Mocker()
    
    override func setUp() {
        self.networkService = NetworkServiceStub(stub: .login)
        self.interactor = AuthInteractor(with: networkService)
        self.viewModel = SignInViewModel(interactor)
    }
    
    func testLogin() {
        viewModel.login(user: "valid", pass: "valid") { (isLogged) in
            XCTAssertTrue(isLogged)
        }
    }
    
    func testUserLoginFail() {
        guard let service = self.networkService as? NetworkServiceStub else {
            assertionFailure()
            return
        }
        
        service.changeStub(stub: .login_fail)
        
        viewModel.login(user: "invalid", pass: "invalid") { (isLogged) in
            XCTAssertFalse(isLogged)
        }
    }
}
