
//
//  HomeViewModelTest.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import XCTest
@testable import HeyBeach

class HomeViewModelTests: XCTestCase {
    
    var networkService: INetworkService!
    var interactor: IHomeInteractor!
    var viewModel: IHomeViewModel!
    let mocker = Mocker()
    
    override func setUp() {
        self.networkService = NetworkServiceStub(stub: .beaches_page_1)
        self.interactor = HomeInteractor(with: networkService)
        self.viewModel = HomeViewModel(interactor)
    }
    
    func testFirstFetch() {
        viewModel.fetchData {
            let items = self.viewModel.beaches
            guard let expected: [Beach] = self.mocker.mock(stub: StubJson.beaches_page_1) else {
                assertionFailure()
                return
            }
            
            XCTAssertEqual(expected.count, items.count)
        }
    }
    
    func testAppendItemsWhenFetch() {
        viewModel.fetchData {
            guard let expected: [Beach] = self.mocker.mock(stub: StubJson.beaches_page_1) else {
                assertionFailure()
                return
            }
            
            
            self.viewModel.fetchData {
                let items = self.viewModel.beaches
                XCTAssertEqual(expected.count * 2, items.count)
            }
        }
    }
    
    func testFetchTwoPages() {
        viewModel.fetchData {
            guard let pageOne: [Beach] = self.mocker.mock(stub: StubJson.beaches_page_1) else {
                assertionFailure()
                return
            }
            
            guard let service = self.networkService as? NetworkServiceStub else {
                assertionFailure()
                return
            }
            
            service.changeStub(stub: .beaches_page_2)
            
            self.viewModel.fetchData {
                guard let pageTwo: [Beach] = self.mocker.mock(stub: StubJson.beaches_page_1) else {
                    assertionFailure()
                    return
                }
                var expected: [Beach] = []
                expected.append(contentsOf: pageOne)
                expected.append(contentsOf: pageTwo)
                
                let items = self.viewModel.beaches
                XCTAssertEqual(expected.count, items.count)
            }
        }
    }
}
