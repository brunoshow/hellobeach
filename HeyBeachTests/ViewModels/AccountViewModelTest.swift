//
//  AccountViewModelTest.swift
//  HeyBeachTests
//
//  Created by Bruno Rosa on 1/16/19.
//  Copyright © 2019 Bruno Show. All rights reserved.
//

import XCTest
@testable import HeyBeach

class AccountViewModelTests: XCTestCase {
    
    private var networkService: INetworkService!
    private var interactor: IAccountInteractor!
    private var viewModel: IAccountViewModel!
    private let mocker = Mocker()
    
    override func setUp() {
        self.networkService = NetworkServiceStub(stub: .user)
        self.interactor = AccountInteractor(with: networkService)
        self.viewModel = AccountViewModel(interactor)
    }
    
    func testUserFetch() {
        viewModel.fetchUser {
            let user = self.viewModel.user
            guard let expected: User = self.mocker.mock(stub: .user) else {
                assertionFailure()
                return
            }
            
            XCTAssertNotNil(user)
            XCTAssertEqual(expected.id, user?.id)
            XCTAssertEqual(expected.email, user?.email)
        }
    }
    
    func testUserFetchFail() {
        guard let service = self.networkService as? NetworkServiceStub else {
            assertionFailure()
            return
        }
        
        service.changeStub(stub: .user_fail)
        
        viewModel.fetchUser {
            let user = self.viewModel.user
            
            XCTAssertNil(user)
        }
    }
    
    func testLogout() {
        viewModel.fetchUser {}
        guard let service = self.networkService as? NetworkServiceStub else {
            assertionFailure()
            return
        }
        
        service.changeStub(stub: .logout)
        viewModel.logout(completion: { (success) in
            XCTAssertTrue(success)
            XCTAssertNil(self.viewModel.user)
        })
    }
    
    func testLogoutFail() {
        viewModel.fetchUser {}
        guard let service = self.networkService as? NetworkServiceStub else {
            assertionFailure()
            return
        }
        
        service.changeStub(stub: .logout_fail)
        
        viewModel.logout(completion: { (success) in
            XCTAssertFalse(success)
            XCTAssertNotNil(self.viewModel.user)
        })
    }
}
